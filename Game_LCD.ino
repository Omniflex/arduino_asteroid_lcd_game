#include <LiquidCrystal.h>
LiquidCrystal lcd(12,11,5,4,3,2);

int board[2][16];
const int framerate = 60;
const float period = (1000/framerate);
unsigned long time_prev = millis();
int pos = 1;
unsigned long framenumber = 0;
bool GO = false;
int refresh = 60;
int best = 0;
int distance = 0;


bool reset(){
  lcd.clear();
  lcd.print("Distance : ");
  lcd.setCursor(0,1);
  lcd.print(distance);
  delay(2000);
  lcd.clear();
  if (best < distance){
    best = distance;
  }
  lcd.print("Meilleur : ");
  lcd.setCursor(0,1);
  lcd.print(best);
  delay(2000);
  framenumber = 0;
  pos = 1;
  refresh = 60;
  distance = 0;
  for(int i=0;i<15;i++){
    board[0][i]=0;
    board[1][i]=0;
  }
  return true;
}

bool collision(){
  if (board[pos][0] == 1){
    lcd.setCursor(0, pos);
    lcd.print("*");
    delay(1000);
    return reset();
  }
  return false;
}

void blockup(){
  board[0][15] = 1;
}

void blockdown(){
  board[1][15] = 1;
}

void newblock(){
  int enemy = random(5);
  switch(enemy){
    case 0 : break;
    case 3 : break;
    case 4 : break;
    case 1 : {
      if (board[1][14] == 0){
      blockup();
      }
      break;
    }
    case 2 : {
      if (board[0][14] == 0){
      blockdown();
      }
      break;
    }
  }
  Serial.print(board[0][14]);
  Serial.print("\n");
}

void updateboard(){
  for (int i=0;i<15;i++){
    board[0][i] = board[0][i+1];
    board[1][i] = board[1][i+1];
  }
  board[0][15] = 0;
  board[1][15] = 0;
}

void drawboard(){

  lcd.clear();

  for (int i=0;i<16;i++){
    if(board[0][i] == 1){
      lcd.setCursor(i,0);
      lcd.print("O");
    }
    if(board[1][i] == 1){
      lcd.setCursor(i,1);
      lcd.print("O");
    }
  }
}

void drawplayer(){
  lcd.setCursor(0,pos);
  lcd.print(">");
}


void setup() {
  //Set random seed
  randomSeed(analogRead(0));

  //Init board array
  for(int i=0;i<15;i++){
  board[0][i]=0;
  board[1][i]=0;
  }

  //Init LCD screen and buttons
  lcd.begin(16,2);
  pinMode(8, INPUT);
  pinMode(9,INPUT);
  Serial.begin(9600);

  //Prompt
  lcd.print("Debut du jeu");
  delay(1000);
  lcd.clear();

  //Player
  lcd.setCursor(0,1);
  lcd.print(">");
}

void loop() {

  framenumber++;
  unsigned long time_beg = millis();
//  Serial.print(1000/(time_beg-time_prev));
//  Serial.print("\n");
  time_prev = time_beg;

  //Move DOWN
  if (digitalRead(9) == HIGH){
    pos = 1;
    drawboard();
    drawplayer();
  }

  //Move UP
  if (digitalRead(8) == HIGH){
    pos = 0;
    drawboard();
    drawplayer();
  }

 GO = collision();

  if (framenumber%600==599){
    refresh = refresh - 5;
  }


  if (framenumber%refresh==0){
    distance++;
    updateboard();
    newblock();
    drawboard();
    drawplayer();
  }

  if (GO == false){
    unsigned long time_end = millis();
    int time_frame = time_end-time_beg;
    delay(period - time_frame);
  }
}
