[CONCEPT]
This is the code for a simple arduino game made in the fashion of two button games
of the past. The goal is to survive for as long as possible by dodging asteroids 

[GAMEPLAY]
The player controlls a little ship that need to dodge asteroids coming at him in order to
survive. He can go up or down on two lanes of actions. Each time you die, you restart your
journey to from the beginning with newly generated obstacles. The game also keeps track of 
your best score.

[IMPLEMENTATION]
This game needs two button and a 16x2 LCD screen, although these dimensions can
be changed in the code. Arduino's LiquidCrystal library is used, and the wiring follows
the first LCD example shown in the Arduino UNO beginner guide. Don't forget the pull-up
resistors and leave analog pin 0 unwired in order to use a different random seed each time.
The current implementation assures that a safe way always exists.

[IMPROVEMENTS]
A clean refactor would be a good idea : putting every function in a separate file could
render the .ino more readable, but as the project is short, this shouldn't be an issue.
Arduino's beginner pack LCD has a noticeable afterglow that impedes gameplay when speed
becomes too large. Rare freezes can occur, but no source has been idenified yet. Adding 
some sort of rocket to shoot and disabling the existence of the safe route would be great!

[LICENSE]
This little project is under GNU GPL, so feel free to use it as you want!

[CONTACT]
omniflex@outlook.fr
